package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class LeilaoTeste {

    Usuario usuario;
    Lance lance;
    Leilao leilao;

    @BeforeEach
    public void setUp() {
        this.usuario = new Usuario(1,"Alder");
        this.lance = new Lance(usuario, 15.00);

        List<Lance> lances = new ArrayList<>();
        lances.add(lance);

        leilao = new Leilao(lances);
    }

    @Test
    public void testarValidarLancePositivo(){
        Lance lance = new Lance(this.usuario, 20.00);
        Lance resposta = leilao.validarLance(lance);

        Assertions.assertSame(lance, resposta);
    }

    @Test
    public void testarValidarLanceNegativo(){
        Lance lance = new Lance(this.usuario, 9.00);

        Assertions.assertThrows(RuntimeException.class, () -> {
            leilao.validarLance(lance);
        });
    }


    @Test
    public void testarAdicionarNovoLancePositivo() {
        Lance lance = new Lance(usuario, 20.0);

        Lance retorno =  leilao.adicionarNovoLance(lance);

        Assertions.assertSame(lance, retorno);

        Assertions.assertEquals(true, leilao.getLances().contains(lance));
    }

    @Test
    public void testarAdicionarNovoLanceNegativo() {
        Lance lance = new Lance(usuario, 5.0);

        Assertions.assertThrows(RuntimeException.class, () -> {
            leilao.adicionarNovoLance(lance);
        });

        Assertions.assertEquals(false, leilao.getLances().contains(lance));

    }
}
