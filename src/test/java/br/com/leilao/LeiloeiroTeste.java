package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class LeiloeiroTeste {

    Usuario usuario;
    Lance lance;
    Leilao leilao;
    Leiloeiro leiloeiro;

    @BeforeEach
    public void setUp() {
        this.usuario = new Usuario(1,"Alder");
        this.lance = new Lance(usuario, 15.00);

        Lance lanceMenor = new Lance(usuario, 9.00);

        List<Lance> lances = new ArrayList<>();
        lances.add(lance);
        lances.add(lanceMenor);

        leilao = new Leilao(lances);

        leiloeiro = new Leiloeiro("Bruna", leilao);
    }

    @Test
    public void testarRetornarMaiorLance(){
        Lance lance = leiloeiro.retornarMaiorLance();

        Assertions.assertSame(this.lance, lance);

    }

    @Test
    public void testarRetornarMaiorLanceListaVazia(){
        List<Lance> lances = new ArrayList<>();
        leilao.setLances(lances);
        leiloeiro.setLeilao(leilao);

        Assertions.assertThrows(RuntimeException.class, () -> {
            leiloeiro.retornarMaiorLance();
        });

    }
}
