package br.com.leilao;

import java.util.List;

public class Leilao {

    private List<Lance> lances;

    public Leilao() {
    }

    public Leilao(List<Lance> listaLances) {
        this.lances = listaLances;
    }

    public List<Lance> getLances() {
        return lances;
    }

    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }

    public Lance validarLance(Lance lance){

        for (Lance item: this.lances) {
            if(lance.getValorDoLance() < item.getValorDoLance()){
                throw new RuntimeException("Lance menor que o anterior");
            }
        }
        return lance;
    }

    public Lance adicionarNovoLance(Lance lance) {

        Lance lanceObjeto = validarLance(lance);

        this.lances.add(lanceObjeto);

        return lance;

    }

}
