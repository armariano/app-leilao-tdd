package br.com.leilao;

import java.util.Collections;
import java.util.List;

public class Leiloeiro {
    private String nome;
    private Leilao leilao;

    public Leiloeiro() {
    }

    public Leiloeiro(String nome, Leilao leiao) {
        this.nome = nome;
        this.leilao = leiao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }

    public Lance retornarMaiorLance() {
        List<Lance> lances = this.leilao.getLances();

        if(lances.size() == 0){
            throw new RuntimeException("Lista de lances vázia");
        }
        Lance lance = lances.get(0);

        for (Lance item : lances) {
            if (lance.getValorDoLance() < item.getValorDoLance()) {
                lance = item;
            }
        }
        return lance;
    }
}
