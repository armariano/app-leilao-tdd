package br.com.leilao;

import java.text.DecimalFormat;

public class Lance {

    private Usuario usuario;
    private Double valorDoLance;

    public Lance() {
    }

    public Lance(Usuario usuario, Double valorDoLance) {
        this.usuario = usuario;
        this.valorDoLance = valorDoLance;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Double getValorDoLance() {
        return valorDoLance;
    }

    public void setValorDoLance(Double valorDoLance) {
        this.valorDoLance = valorDoLance;
    }
}
